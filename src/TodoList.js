import React from 'react';


class TodoList extends React.Component {
  render() {
    return (
      <ol style={{ textAlign: 'left' }}>
        {
          this.props.list.map((todo, index) => {
            return (
              <li
                style={{ textDecoration: todo.done ? 'line-through' : 'none' }}
                onClick={this.props.handleToggle(index)}
              >
                {todo.title}
              </li>
            );
          })
        }
      </ol>
    )
  }
}

/*
const TodoList = (props) => {
}
*/


export default TodoList;