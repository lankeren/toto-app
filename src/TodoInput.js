import React from 'react';

const TodoInput = (props) => (
  <input
    style={{ border: '1px solid' }}
    value={props.value}
    onKeyDown={props.onKeyDown}
    onChange={props.onChange}
  />
)

export default TodoInput;