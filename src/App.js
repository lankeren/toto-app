import React, { Component } from 'react';
import logo from './logo.svg';
import TodoList from './TodoList';
import TodoInput from './TodoInput';
import './App.css';

class App extends Component {
  state = {
    title: '',
    list: [
      {
        title: 'todo1',
        done: false
      },
      {
        title: '打台球',
        done: false
      }]
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Hello React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <TodoInput
          value={this.state.title}
          onKeyDown={this.handleConfirm}
          onChange={this.handleChange}
         />
         <TodoList
          list={this.state.list}
          handleToggle={this.handleToggle}
        />
      </div>
    );
  }

  handleToggle = (index) => () => {
    this.toggle(index);
  }

  handleChange = e => {
    this.setState({
      title: e.target.value
    });
  }

  handleConfirm = e => {
    if (e.key === 'Enter') {
      const list = [...this.state.list, {
        title: this.state.title,
        done: false
      }];
      this.setState({ list });
    }
  }

  toggle = (index) => {
    const list = this.state.list;
    const target = list[index];
    target.done = !target.done;
    const newList = [
      ...list.slice(0, index),
      target,
      ...list.slice(index + 1)
    ];
    this.setState({
      list: newList
    })
  }
}

export default App;
